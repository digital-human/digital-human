import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Icon } from 'react-native-elements';
import { TouchableOpacity, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import Home from './src/components/Home';
import Lex from './src/components/Lex';
import Bookmarks from './src/components/Bookmarks';
import { navigate, navigationRef } from './src/navigation/RootNavigation';
import store from './src/stores/store';
import LexHeader from './src/components/LexHeader';
import colours from './src/config/colours';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <StatusBar
          backgroundColor={colours.darkBlue}
          barStyle="light-content"
        />
        <Stack.Navigator
          screenOptions={{
            headerShadowVisible: false,
            headerTitle: '',
            headerTintColor: colours.darkBlue,
          }}
        >
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerStyle: {
                backgroundColor: colours.darkBlue,
              },
              headerRight: () => (
                <TouchableOpacity onPress={() => navigate('Bookmarks')}>
                  <Icon name="bookmark" color={colours.lightBlue} size={32} />
                </TouchableOpacity>
              ),
            }}
          />
          <Stack.Screen
            name='Bookmarks'
            component={Bookmarks}
            options={{ headerTitle:'Bookmarks' }}
          />
          <Stack.Screen
            name="Lex"
            component={Lex}
            options={{
              headerRight: () => <LexHeader />,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
