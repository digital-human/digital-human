/**
 * @format
 */

import { AppRegistry } from 'react-native';
import Amplify from 'aws-amplify';
import App from './App';
import { name as appName } from './app.json';
import config from './src/aws-exports';

Amplify.configure(config);

Amplify.configure({
  Auth: {
    identityPoolId: 'ap-southeast-2:7b9d9c26-11dd-4761-b958-eb08f7b58df9',
    region: 'ap-southeast-2',
  },
  Interactions: {
    bots: {
      DigitalHumanBot: {
        name: 'DigitalHumanBot',
        alias: 'Beta',
        region: 'ap-southeast-2',
      },
    },
  },
});

AppRegistry.registerComponent(appName, () => App);
