import * as React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react-native';
import Home from '../src/components/Home';
import App from '../App';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

describe('Testing snapshot rendering', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<Home />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
