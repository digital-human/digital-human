# Digital Human

# Build Instructions

## React Native

Ensure you have correctly set up your development environment: https://reactnative.dev/docs/environment-setup

**Android: **

Before running the application you will need to ensure Metro is running with the `react-native start` command.

```bash
digital-human$ npx react-native start
```

Let Metro Bundler run in it own terminal. Open a new terminal inside our React Native project and run the following command.

```bash
digital-human$ npx react-native run-android
```

**iOS: **

Before running the application you will need to ensure Metro is running with the `react-native start` command.

```bash
digital-human$ npx react-native start
```

Let Metro Bundler run in it own terminal. Open a new terminal inside our React Native project and run the following command.

```bash
digital-human$ npx react-native run-ios
```

## AWS Amplify

Ensure you have AWS CLI installed on your PC: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

Ensure your AWS credentials are set up: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

Pull our amplify application with the `amplify pull` command

```bash
digital-human$ amplify pull --appId d2nzdak3ox529o --envName dev
```

When running our react native application you should now be able to interact with our Lex bot.

## Troubleshooting
if you are getting errors for duplicate output files which prevent you from launching the app, then you'll need to:

1. open ios/DigitalHuman.xcodeproj
2. from XCode navigate to Build Phases
3. expand Copy Bundle Resources & delete all of the .tff files 

if the iOS sim is not able to find or use swift auto-linked libraries such as 'swift_Concurrency', 'swiftFileProvider', 'swiftDataDetection', etc:

1. open ios/DigitalHuman.xcodeproj
2. create a new Swift File 
3. upon saving/creating the file you'll be prompted to create a Bridging-Header, select yes.

if you are getting error code=405: Failed to launch the app on simulator, then:

1. Quit the simulator & relaunch the app.

if you are getting errors for regarding the target deployment of packages you'll need to:

1. open ios/Pods/Pods.xcodeproj
2. from XCode navigate to Build Settings
3. for each package that has triggered this warning, expand Deployment & set the iOS Deployment Target accordingly (error log will specify apropriate ranges, e.g, iOs 9.0 - 11)


