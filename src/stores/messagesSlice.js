import { createSlice } from '@reduxjs/toolkit';

// Default Lex message
const initialState = [
  {
    id: '1',
    message: 'Hello, what can I do for you?',
    sender: 'Lex',
    type: 'Standard',
  },
];

const messagesSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    /**
     * Appends a new message to the Lex instance chat history
     * @param {*} state existing state of the object-tree.
     * @param {*} action a Message object.
     * @returns an updated object-tree.
     */
    addMessage(state, action) {
      const newState = [action.payload].concat(state);
      return newState;
    },

    /**
     * Clears the chat history
     * @param {*} state existing state of the object-tree
     * @param {*} action not used? 
     * @returns 
     */
    clearMessages(state, action) {
      return initialState;
    },

    /**
     * Updates bookmark UI icons to indicate whether a result has been bookmarked or not.
     * @param {*} state existing state of the object-tree.
     * @param {*} action an updated object-tree.
     */
    toggleSave(state, action){
      const message = state.filter(
        item => item.id == action.payload
      )
      message[0].saved = !message[0].saved
    },

    toggleChecklist(state, action) {
      const message = state.find(
        (message) => message.id === action.payload.messageId
      );
      const checklistItem = message.checklistItemObjects.find(
        (checklistItem) =>
          checklistItem.message === action.payload.checklistMessage
      );
      if (checklistItem) {
        checklistItem.checked = action.payload.newValue;
      }
    },
  
    /**
     * handles bookmark presses. This reducer updates the dialogue instance to load the requested interaction.
     * @param {*} state existing state of the object-tree.
     * @param {*} action payload contains an object which represent a saved dialogue between Lex & the user.
     * @returns an updated object-tree
     */
    loadShortcut(state, action){
      // 2 messages: Query Sent followed by Lex Response
      const newState = [action.payload[1]].concat([action.payload[0]]) 
      return newState
    }
  },
});

export const { addMessage, clearMessages, toggleChecklist, toggleSave, loadShortcut } =
  messagesSlice.actions;

export default messagesSlice.reducer;
