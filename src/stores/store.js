import { configureStore } from '@reduxjs/toolkit';
import messsagesReducer from './messagesSlice';
import bookmarksReducer from './bookmarksSlice';

export default configureStore({
  reducer: {
    messages: messsagesReducer,
    bookmarks: bookmarksReducer,
  },
});
