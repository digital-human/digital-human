import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    initialised: false,
    bookmarks: []
}

const bookmarksSlice = createSlice({
    name: 'bookmarks',
    initialState,
    reducers: {
        /**
         * This reducer is called once upon App launch. It is responsible for updated Redux with bookmarks retrieved from AsyncStorage (Persistent storage)
         * @param {*} state : existing state of the object-tree.
         * @param {*} action : payload contains all bookmark objects loaded from AsyncStorage.
         * @returns an updated object-tree
         */
        loadBookmarks(state, action){
            const newState = {
                initialised: true,
                bookmarks: action.payload
            }
            return newState
        },
        /**
         * Appends a bookmark to the bookmarks array. Bookmarks are saved to Redux as they are referenced in other components.
         * @param {*} state : existing state of the object-tree.
         * @param {*} action : a Bookmark object.
         */
        saveBookmark(state, action){
            state.bookmarks = state.bookmarks.concat(action.payload)
        },

        /**
         * Removes bookmarks from the bookmarks array. Deletes bookmarks from the store in order to update UI components accordingly.
         * @param {*} state : existing state of the object-tree.
         * @param {*} action : a Bookmark object.
         */
        removeBookmark(state, action){
            state.bookmarks = state.bookmarks.filter(
                item => item.id !== action.payload
            )
        },
        /** 
         * Updates favourite UI icons to indicate whether bookmarks are favourited or not.
         * @param {*} state : existing state of the object-tree
         * @param {*} action : a Bookmark object.
         */
        toggleFavourite(state, action){
            const toggleable = state.bookmarks.filter(
                item => item.id == action.payload
            )
            toggleable[0].favourited = !toggleable[0].favourited
        }
    }
})

export const { loadBookmarks, saveBookmark, removeBookmark, toggleFavourite } = bookmarksSlice.actions

export default bookmarksSlice.reducer