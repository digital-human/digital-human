import { Interactions, Storage } from 'aws-amplify';

export default {
  // sends request to Digital Human Lex bot hosted on AWS and returns response
  async lexInteraction(userInput) {
    const response = await Interactions.send('DigitalHumanBot', userInput);
    return response;
  },
  // requests signed hyperlink of pdf file from s3 bucket for user to access
  async getSignedHyperlinkKey(key) {
    const signedHyperlink = await Storage.get(key);
    return signedHyperlink;
  },
};
