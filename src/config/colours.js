const colours = {
  darkBlue: '#4051E7',
  lightBlue: '#E6E8FC',
};

export default colours;
