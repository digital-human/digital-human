import React, { useState } from 'react';
import { TouchableOpacity, View, StyleSheet, TextInput } from 'react-native';
import { Icon } from 'react-native-elements';
import colours from '../config/colours';

const styles = StyleSheet.create({
  searchBar: {
    padding: 8,
    flexDirection: 'row',
  },
  searchButton: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 0,
    borderLeftColor: 'white',
    backgroundColor: '#f1f1f1',
    flex: 1,
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
  },
  input: {
    color: 'black',
    backgroundColor: '#f1f1f1',
    borderRightWidth: 0,
    borderRightColor: 'white',
    paddingHorizontal: 16,
    fontSize: 16,
    flex: 6,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
  },
});

const InputBar = (props) => {
  const [message, setMessage] = useState('');
  const [inputBarRef, setInputBarRef] = useState();

  const handleSearch = () => {
    if (message.length !== 0) {
      props.sendMessage(message); // callback function to Lex component that handles sending of messages.
      inputBarRef.clear();
      setMessage(''); // clears inputbar after search
    }
  };

  return (
    <View style={styles.searchBar}>
      <TextInput
        ref={(ref) => setInputBarRef(ref)}
        placeholder="What are you after?"
        selectionColor={colours.darkBlue}
        placeholderTextColor="darkgrey"
        clearButtonMode="while-editing"
        onChangeText={(inputQuery) => setMessage(inputQuery)}
        defaultValue={message}
        style={styles.input}
      />
      <TouchableOpacity
        style={styles.searchButton}
        onPress={handleSearch}
        accessibilityHint="searchButton"
      >
        <Icon name="send" color={colours.darkBlue} />
      </TouchableOpacity>
    </View>
  );
};

export default InputBar;
