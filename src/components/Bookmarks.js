import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import { nanoid } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { loadShortcut, toggleSave, addMessage } from '../stores/messagesSlice';
import { removeBookmark, toggleFavourite } from '../stores/bookmarksSlice';
import * as RootNavigation from '../navigation/RootNavigation';
import services from '../stores/services';
import colours from '../config/colours';

const styles = StyleSheet.create({
  backContainer: {
    backgroundColor: 'white',
    flex: 1,
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    color: colours.lightBlue,
  },
  title: {
    fontSize: 15,
    color: colours.darkBlue,
  },
  iconDelete: {
    position: 'absolute',
    top: -38,
    right: -15,
  },
  iconFavourite: {
    position: 'absolute',
    bottom: -18,
    right: -15,
  },
});

/**
 * This component manages a bookmarks store.
 * This component allows users to:
 * 1. repeat a previously saved search which is handled by Lex.
 * 2. delete a bookmark from the store.
 * 3. favourite a bookmark so that it can be accessed from the Home component, these limits can be adjusted (default set to 3)
 */
const Bookmarks = () => {
  const bookmarks = useSelector((state) => state.bookmarks);
  // console.log('bookmarks in Redux:', bookmarks)

  const dispatch = useDispatch();

  /**
   * Deletes the bookmark by:
   * 1. Removing it from AsyncStorage (Persistent Storage).
   * 2. Removing it from Redux (Temporary Storage)
   * 3. toggling the 'saved' indicator off in case the bookmarked result still remains within the Lex instance
   * @param {*} bookmark: selected bookmark for deletion
   */
  const deleteBookmark = (bookmark) => {
    try {
      AsyncStorage.removeItem(bookmark.id);
      dispatch(removeBookmark(bookmark.id));
      dispatch(toggleSave(bookmark.id));
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Allows users to add shortcuts to the Home View by favouriting/unfavouriting bookmarks from the bookmark store
   * @param {*} bookmark : selected bookmark for favourite/unfavourite
   * @param {Number} limit: user-specified limit for how many bookmarks can be shown in the Home view,
   */
  const favouritesHandler = async (bookmark, limit) => {
    const favourites = bookmarks.bookmarks.filter(
      (item) => item.favourited == true
    );

    if (favourites.length < limit || favourites.includes(bookmark)) {
      dispatch(toggleFavourite(bookmark.id));
    } else {
      console.log('shortcuts limit exceeded!');
    }

    // We want to ensure that the favourited state persists after the App has shutdown, so we update the 'favourited' state
    try {
      const contents = {
        id: bookmark.id,
        savedAs: bookmark.savedAs,
        intentName: bookmark.intentName,
        slots: bookmark.slots,
        favourited: !bookmark.favourited,
      };
      await AsyncStorage.setItem(bookmark.id, JSON.stringify(contents)).then(
        console.log('AsyncStore updated!')
      );
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Navigate to Chat instance
   */
  const lexNavigation = () => {
    RootNavigation.navigate('Lex');
  };

  /**
   * initiates a new chat instance when a bookmark has been selected.
   * Sends off a request to Lex which to follow through with a search
   * @param {*} bookmark : selected bookmark to load up
   */
  function handleSearch(bookmark) {
    const query = bookmark.futurePassingQuery;
    console.log(query);
    services.lexInteraction(query).then((response) => {
      // Check the 'type' of shortcut loaded to ensure correct UI render (i.e., is it a checklist, does it contain a hyperlink?)

      // is it a checklist?
      if (response.intentName === 'SpecialistQuery') {
        const checklistItems = response.message.split('\n');
        const checklistMessage = checklistItems.shift();
        const phoneNumber = checklistItems.pop().replace('Phone :', '');
        const numberRegExp = new RegExp('[0-9].*');
        let note = '';

        if (!numberRegExp.test(checklistItems[checklistItems.length - 1])) {
          // Note provided with checklist
          note = checklistItems.pop();
        }

        const checklistItemObjects = checklistItems.map(function (value) {
          return { message: value, checked: false };
        });

        dispatch(
          // Adds lex response to Redux store.
          loadShortcut([
            {
              id: nanoid(),
              message: query,
              sender: 'User',
              type: 'Standard',
            },
            {
              id: bookmark.id,
              message: checklistMessage,
              sender: 'Lex',
              type: 'Checklist',
              status:
                response.dialogState === 'Fulfilled'
                  ? 'Fulfilled'
                  : 'InProgress',
              saved: true,
              checklistItemObjects,
              phoneNumber,
              note,
            },
          ])
        );
        // now to check whether we're loading up hyperlinked response...
      } else if (
        response.message.endsWith('.pdf') ||
        response.message.endsWith('.pdf\n')
      ) {
        const messageLines = response.message.split('\n');
        let appendedPdfs = [];
        // Extracts all pdf file names from provided links
        while (
          messageLines[messageLines.length - 1] !==
            'For additional information: ' &&
          messageLines[messageLines.length - 1] !==
            'Here are some documents for you to review:'
        ) {
          const pdfLink = messageLines.pop();
          appendedPdfs.push(pdfLink.slice(pdfLink.lastIndexOf('/') + 1));
        }
        appendedPdfs = appendedPdfs.reverse();

        if (response.message.endsWith('.pdf\n')) {
          appendedPdfs.pop();
        }

        let lastIndex;
        if (
          messageLines[messageLines.length - 1] ===
          'For additional information: '
        ) {
          lastIndex =
            response.message.lastIndexOf('For additional information:') + 27;
        } else {
          lastIndex =
            response.message.lastIndexOf(
              'Here are some documents for you to review:'
            ) + 42;
        }

        const responseMessage = response.message.slice(0, lastIndex);

        dispatch(
          // Adds lex response to Redux store.
          loadShortcut([
            {
              id: nanoid(),
              message: query,
              sender: 'User',
              type: 'Standard',
            },
            {
              id: bookmark.id,
              message: responseMessage,
              sender: 'Lex',
              type: 'Hyperlinks',
              appendedPdfs,
              status:
                response.dialogState == 'Fulfilled'
                  ? 'Fulfilled'
                  : 'InProgress',
              intentName: response.intentName,
              slots: response.slots,
              saved: true,
            },
          ])
        );

        // Leaving this else clause in place for when 'responses without pdf links' are available. Untill then, this clause won't register.
      } else {
        dispatch(
          loadShortcut([
            {
              id: nanoid(),
              message: query,
              sender: 'User',
              type: 'Standard',
            },
            {
              id: bookmark.id,
              message: response.message,
              sender: 'Lex',
              type: 'Standard',
              status:
                response.dialogState === 'Fulfilled'
                  ? 'Fulfilled'
                  : 'InProgress',
              intentName: response.IntentName,
              slots: response.slots,
              saved: true,
            },
          ])
        );
      }
      if (
        response.intentName === 'SpecificQuery' &&
        response.dialogState === 'Fulfilled' &&
        response.slots.WardCall !== undefined
      ) {
        let suggestions = [];

        switch (response.slots.SpecificQueryType) {
          case 'Causes':
            suggestions = ['Worrying Factors', 'Immediate Management'];
            break;
          case 'Worrying factors':
            suggestions = ['Causes', 'Immediate Management'];
            break;
          case 'Immediate management':
            suggestions = ['Causes', 'Worrying Factors'];
            break;
          default:
            suggestions = [
              'Causes',
              'Worrying Factors',
              'Immediate Management',
            ];
        }

        dispatch(
          addMessage({
            id: nanoid(),
            message: `Is there anything else you would like to know about ${response.slots.WardCall}?`,
            sender: 'Lex',
            type: 'SuggestionsCard',
            wardCall: response.slots.WardCall,
            options: suggestions,
          })
        );
      }
      lexNavigation();
    });
  }

  /**
   * Bookmark Components which allow users to:
   * 1. press to initiate a previously saved search
   * 2. delete bookmarks by pressing a cross located in the top-left.
   * 3. favourite a bookmark for quick-access from the Home component by pressing a heart-icon located in the bottom-left.
   * @param {*} param0 a Bookmark object
   * @returns a pressable bookmark
   */
  const Item = ({ item, backgroundColor }) => (
    <View>
      <View style={[styles.item, backgroundColor]}>
        <TouchableOpacity onPress={() => handleSearch(item)}>
          <Text style={styles.title}>{item.savedAs}</Text>
        </TouchableOpacity>
        <View>
          <TouchableOpacity
            style={styles.iconDelete}
            onPress={() => deleteBookmark(item)}
          >
            <Icon
              name="cross"
              type="entypo"
              size={20}
              color={colours.darkBlue}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconFavourite}
            onPress={() => favouritesHandler(item, 5)}
          >
            <Icon
              name={item.favourited ? 'md-heart-sharp' : 'md-heart-outline'}
              type="ionicon"
              size={20}
              color={item.favourited ? 'red' : colours.darkBlue}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  /**
   * Render function used by the FlatList Component
   * @param {*} param0  a Bookmark object
   * @returns a pressable bookmark
   */
  const renderBookmark = ({ item }) => {
    console.log(`rendering: item.id=${item.id}`);
    const backgroundColor = colours.lightBlue;
    return <Item item={item} backgroundColor={{ backgroundColor }} />;
  };

  if (bookmarks.bookmarks.length > 0) {
    return (
      <View style={styles.backContainer}>
        <SafeAreaView>
          <FlatList
            data={bookmarks.bookmarks}
            renderItem={renderBookmark}
            keyExtractor={(item) => item.id}
          />
        </SafeAreaView>
      </View>
    );
  }
  return (
    <View style={styles.backContainer}>
      <SafeAreaView style={styles.container}>
        <Text style={{ alignSelf: 'center', bottom: -250 }}>
          No bookmarks saved
        </Text>
      </SafeAreaView>
    </View>
  );
};

export default Bookmarks;
