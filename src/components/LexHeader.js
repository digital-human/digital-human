import React from 'react';
import { TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';
import { Icon } from 'react-native-elements';
import { clearMessages } from '../stores/messagesSlice';
import colours from '../config/colours';

const LexHeader = () => {
  const dispatch = useDispatch();

  const deleteChat = () => {
    dispatch(clearMessages());
  };
  return (
    <TouchableOpacity onPress={deleteChat}>
      <Icon name="refresh" color={colours.darkBlue} size={32} />
    </TouchableOpacity>
  );
};

export default LexHeader;
