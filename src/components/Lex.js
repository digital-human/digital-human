/* eslint-disable no-nested-ternary */
import React, { useRef } from 'react';
import {
  View,
  StyleSheet,
  Text,
  KeyboardAvoidingView,
  FlatList,
  TouchableOpacity,
  Keyboard,
  Linking,
  SafeAreaView,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { nanoid } from '@reduxjs/toolkit';
import CheckBox from '@react-native-community/checkbox';
import { Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Dialog from 'react-native-dialog';
import InputBar from './InputBar';
import colours from '../config/colours';
import {
  addMessage,
  toggleChecklist,
  toggleSave,
} from '../stores/messagesSlice';
import { saveBookmark, removeBookmark } from '../stores/bookmarksSlice';
import services from '../stores/services';

const styles = StyleSheet.create({
  lexContainer: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  textContainer: {
    flex: 1,
  },
  messageContainer: {
    padding: 8,
  },
  lexResponseContainer: {
    backgroundColor: '#f1f1f1',
    padding: 12,
    alignSelf: 'flex-start',
    borderTopEndRadius: 30,
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,
    maxWidth: '80%',
  },
  userQueryContainer: {
    backgroundColor: colours.darkBlue,
    padding: 12,
    alignSelf: 'flex-end',
    borderTopStartRadius: 30,
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,
    maxWidth: '80%',
  },
  lexResponseText: {
    fontSize: 16,
    color: 'black',
  },
  userQueryText: {
    fontSize: 16,
    color: 'white',
  },
  suggestionsButtonContainer: {
    paddingTop: 8,
  },
  suggestionsButton: {
    alignItems: 'center',
    padding: 8,
    borderRadius: 30,
    backgroundColor: 'lightgrey',
  },
  lexResponseButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  defaultBookmarkButton: {
    alignItems: 'center',
    padding: 5,
    borderRadius: 30,
    backgroundColor: 'lightgrey',
    flex: 1,
  },
  checklistResponseButton: {
    alignItems: 'center',
    padding: 8,
    borderRadius: 30,
    backgroundColor: 'lightgrey',
    minWidth: '45%',
  },
  linkText: {
    textDecorationLine: 'underline',
    fontSize: 16,
    color: colours.darkBlue,
  },
  checklistContainer: {
    padding: 8,
    flexDirection: 'row',
  },
  checklistText: {
    flex: 4,
  },
  checklistBox: {
    alignItems: 'center',
    flex: 1,
  },
  bookmark: {
    position: 'absolute',
    bottom: -5,
    right: 10,
  },
  checklistBookmark: {
    position: 'absolute',
    bottom: -18,
    right: 5,
  },
});

const Lex = () => {
  const scrollViewRef = useRef();
  const messages = useSelector((state) => state.messages); // Uses Redux to manage chat data
  const [saveAs, setSaveAs] = React.useState('');
  const [visible, setVisible] = React.useState(false);
  const [selected, selectBookmark] = React.useState('');

  const dispatch = useDispatch(); // Used to utilize functions within our Redux store

  // Method to send user message and interact with AWS Lex bot
  const sendMessage = (message) => {
    dispatch(
      // Adds user message to Redux store.
      addMessage({
        id: nanoid(),
        message,
        sender: 'User',
        type: 'Standard',
      })
    );

    services.lexInteraction(message).then((response) => {
      if (
        // What we're checking for here is to see whether the response is to a checklist Query.
        response.intentName === 'SpecialistQuery' &&
        response.dialogState === 'Fulfilled' &&
        response.message !==
          `Sorry we do not have information for a ${response.slots.SpecialistCall} specialist.`
      ) {
        Keyboard.dismiss(); // Hides keyboard automatically for readability purposes.
        const checklistItems = response.message.split('\n');
        const checklistMessage = checklistItems.shift();
        const phoneNumber = checklistItems.pop().replace('Phone: ', '');
        const numberRegExp = new RegExp('[0-9].*');
        let note = '';

        if (!numberRegExp.test(checklistItems[checklistItems.length - 1])) {
          // Note provided with checklist
          note = checklistItems.pop();
        }

        const checklistItemObjects = checklistItems.map(function (value) {
          return { message: value, checked: false };
        });

        dispatch(
          // Adds lex response to Redux store.
          addMessage({
            id: nanoid(),
            message: checklistMessage,
            sender: 'Lex',
            type: 'Checklist',
            status:
              response.dialogState === 'Fulfilled' ? 'Fulfilled' : 'InProgress',
            checklistItemObjects,
            phoneNumber,
            note,
            slots: response.slots,
          })
        );
      } else if (
        response.message.endsWith('.pdf') ||
        response.message.endsWith('.pdf\n')
      ) {
        const messageLines = response.message.split('\n');
        let appendedPdfs = [];
        // Extracts all pdf file names from provided links
        while (
          messageLines[messageLines.length - 1] !==
            'For additional information: ' &&
          messageLines[messageLines.length - 1] !==
            'Here are some documents for you to review:'
        ) {
          const pdfLink = messageLines.pop();
          appendedPdfs.push(pdfLink.slice(pdfLink.lastIndexOf('/') + 1));
        }

        appendedPdfs = appendedPdfs.reverse();

        if (response.message.endsWith('.pdf\n')) {
          appendedPdfs.pop();
        }

        let lastIndex;
        if (
          messageLines[messageLines.length - 1] ===
          'For additional information: '
        ) {
          lastIndex =
            response.message.lastIndexOf('For additional information:') + 27;
        } else {
          lastIndex =
            response.message.lastIndexOf(
              'Here are some documents for you to review:'
            ) + 42;
        }

        console.log(appendedPdfs);

        const responseMessage = response.message.slice(0, lastIndex);

        dispatch(
          // Adds lex response to Redux store.
          addMessage({
            id: nanoid(),
            message: responseMessage,
            sender: 'Lex',
            type: 'Hyperlinks',
            appendedPdfs,
            status:
              response.dialogState === 'Fulfilled' ? 'Fulfilled' : 'InProgress',
            intentName: response.intentName,
            slots: response.slots,
            saved: false,
          })
        );
      } else {
        dispatch(
          // Adds lex response to Redux store.
          addMessage({
            id: nanoid(),
            message: response.message,
            sender: 'Lex',
            type: 'Standard',
            status:
              response.dialogState === 'Fulfilled' ? 'Fulfilled' : 'InProgress',
            intentName: response.intentName,
            slots: response.slots,
            saved: false,
          })
        );
      }

      if (
        response.intentName === 'SpecificQuery' &&
        response.dialogState === 'Fulfilled' &&
        response.slots.WardCall !== undefined
      ) {
        Keyboard.dismiss(); // Hides keyboard automatically for readability purposes.
        let suggestions = [];

        switch (response.slots.SpecificQueryType) {
          case 'Causes':
            suggestions = ['Worrying Factors', 'Immediate Management'];
            break;
          case 'Worrying factors':
            suggestions = ['Causes', 'Immediate Management'];
            break;
          case 'Immediate management':
            suggestions = ['Causes', 'Worrying Factors'];
            break;
          default:
            suggestions = [
              'Causes',
              'Worrying Factors',
              'Immediate Management',
            ];
        }

        dispatch(
          addMessage({
            id: nanoid(),
            message: `Is there anything else you would like to know about ${response.slots.WardCall}?`,
            sender: 'Lex',
            type: 'SuggestionsCard',
            wardCall: response.slots.WardCall,
            options: suggestions,
          })
        );
      }
    });
  };

  const handleSuggestedQueryPress = (wardCall, queryType) => {
    const message = `${wardCall.toLowerCase()} ${queryType.toLowerCase()}`;
    sendMessage(message);
  };

  const toggleChecklistHandler = (newValue, messageId, checklistMessage) => {
    dispatch(
      toggleChecklist({
        messageId,
        checklistMessage,
        newValue,
      })
    );
  };

  /**
   * Creates a bookmark of a saveable interaction with Lex. A bookmark holds relevant information pertaining to repeating a completed query.
   * @param {*} bookmarkable : a Bookmark object.
   * @param {*} saveAsName : user-specified name for the bookmark
   */
  const bookmark = async (bookmarkable, saveAsName) => {
    let futurePassingQuery = '';

    if (
      bookmarkable.intentName === 'SpecificQuery' &&
      bookmarkable.slots.WardCall !== undefined
    ) {
      for (const slot in bookmarkable.slots) {
        futurePassingQuery += `${bookmarkable.slots[slot]} `;
      }
      console.log(`hi ${futurePassingQuery}`);
    } else if (bookmarkable.intentName === 'SpecialistQuery') {
      futurePassingQuery = `${bookmarkable.slots.SpecialistCall} specialist`;
    } else {
      futurePassingQuery = messages[1].message;
      console.log(futurePassingQuery);
    }

    const save = {
      id: bookmarkable.id,
      savedAs: saveAsName,
      futurePassingQuery,
      favourited: false,
    };

    dispatch(saveBookmark(save));
    dispatch(toggleSave(bookmarkable.id));

    try {
      // TODO: implement a conditional check to prevent users from using the same bookmark name for multiple bookmarks.
      await AsyncStorage.setItem(bookmarkable.id, JSON.stringify(save));
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Deletes a Bookmark from the users Bookmark store.
   * @param {*} bookmarkable : a Bookmark object.
   */
  const unbookmark = (bookmarkable) => {
    dispatch(removeBookmark(bookmarkable.id));
    dispatch(toggleSave(bookmarkable.id));

    try {
      AsyncStorage.removeItem(bookmarkable.id);
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Prompts the user to:
   * 1. enter a name for the bookmark
   * 2. confirm whether they'd like to bookmark the interaction before proceeding with the save.
   * @param {*} bookmarkable
   */
  const showDialog = (bookmarkable) => {
    if (!bookmarkable.saved) {
      setVisible(true);
      selectBookmark(bookmarkable);
    }
  };

  /**
   * Handles Cancel presses on the Dialog pop-up
   */
  const onPressCancel = () => {
    setVisible(false);
  };

  /**
   * Handles Save press on the Dialog pop-up
   */
  const onPressSave = () => {
    bookmark(selected, saveAs);
    selectBookmark('');
    setSaveAs('');
    setVisible(false);
  };

  // Method to query S3 bucket using a signed key from amplify
  const openPdf = (key) => {
    services.getSignedHyperlinkKey(key).then((response) => {
      Linking.openURL(response);
    });
  };

  // Method to render each individual message within chat bot
  const renderMessage = ({ item }) => {
    switch (item.type) {
      case 'SuggestionsCard': {
        const { wardCall } = item;
        return (
          <View>
            <View style={styles.messageContainer}>
              <View style={styles.lexResponseContainer}>
                <Text style={styles.lexResponseText}>{item.message}</Text>
                <FlatList
                  data={item.options}
                  renderItem={({ item: optionItem }) => (
                    <View style={styles.suggestionsButtonContainer}>
                      <TouchableOpacity
                        style={styles.suggestionsButton}
                        onPress={() => {
                          handleSuggestedQueryPress(wardCall, optionItem);
                        }}
                      >
                        <Text style={styles.lexResponseText}>{optionItem}</Text>
                      </TouchableOpacity>
                    </View>
                  )}
                  keyExtractor={(optionItem, index) => index.toString()}
                />
              </View>
            </View>
          </View>
        );
      }
      case 'Checklist': {
        return (
          <View>
            <View style={styles.messageContainer}>
              <View style={styles.lexResponseContainer}>
                <Text style={styles.lexResponseText}>{item.message}</Text>
                <FlatList
                  data={item.checklistItemObjects}
                  renderItem={({ item: checklistItem }) => (
                    <View style={styles.checklistContainer}>
                      <View style={styles.checklistText}>
                        <Text style={styles.lexResponseText}>
                          {checklistItem.message}
                        </Text>
                      </View>
                      <View style={styles.checklistBox}>
                        <CheckBox
                          disabled={false}
                          value={checklistItem.checked}
                          tintColors={{
                            true: 'black',
                            false: 'black',
                          }}
                          onValueChange={(newValue) => {
                            toggleChecklistHandler(
                              newValue,
                              item.id,
                              checklistItem.message
                            );
                          }}
                        />
                      </View>
                    </View>
                  )}
                  keyExtractor={(checklistItem, index) => index.toString()}
                />
                {item.note !== '' && (
                  <Text style={styles.lexResponseText}>{item.note}</Text>
                )}
                <Text />
                <View style={styles.lexResponseButtonContainer}>
                  <TouchableOpacity
                    style={styles.checklistResponseButton}
                    onPress={() => {
                      Linking.openURL(`tel:${item.phoneNumber}`);
                    }}
                  >
                    <Icon name="phone" color="black" />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.checklistResponseButton}
                    onPress={
                      item.saved
                        ? () => unbookmark(item)
                        : () => showDialog(item)
                    }
                  >
                    <Icon
                      name={
                        item.status === 'Fulfilled'
                          ? item.saved
                            ? 'bookmark'
                            : 'bookmark-outline'
                          : null
                      }
                      type="material-community"
                      color="black"
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        );
      }
      case 'Hyperlinks': {
        const modifiedMessage = item.message.replace(/\\n/g, '\n'); // Ensures new lines are captured in messages
        return (
          <View style={styles.messageContainer}>
            <View style={styles.lexResponseContainer}>
              <Text style={styles.lexResponseText}>{modifiedMessage}</Text>
              <FlatList
                data={item.appendedPdfs}
                renderItem={({ item: appendedPdf }) => (
                  <TouchableOpacity onPress={() => openPdf(appendedPdf)}>
                    <Text style={styles.linkText}>{appendedPdf}</Text>
                  </TouchableOpacity>
                )}
                keyExtractor={(appendedPdf, index) => index.toString()}
              />
              <Text />
              <View style={styles.lexResponseButtonContainer}>
                <TouchableOpacity
                  style={styles.defaultBookmarkButton}
                  onPress={
                    item.saved ? () => unbookmark(item) : () => showDialog(item)
                  }
                >
                  <Icon
                    name={
                      item.status === 'Fulfilled'
                        ? item.saved
                          ? 'bookmark'
                          : 'bookmark-outline'
                        : null
                    }
                    type="material-community"
                    color="black"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        );
      }
      default: {
        const modifiedMessage = item.message.replace(/\\n/g, '\n'); // Ensures new lines are captured in messages
        return (
          <View style={styles.messageContainer}>
            <View
              style={
                item.sender === 'Lex'
                  ? styles.lexResponseContainer
                  : styles.userQueryContainer
              }
            >
              <Text
                style={
                  item.sender === 'Lex'
                    ? styles.lexResponseText
                    : styles.userQueryText
                }
                key={item.id}
              >
                {modifiedMessage}
              </Text>
            </View>
          </View>
        );
      }
    }
  };

  return (
    <SafeAreaView style={styles.lexContainer} accessibilityHint="lexScreen">
      <KeyboardAvoidingView style={styles.textContainer}>
        <FlatList
          ref={scrollViewRef}
          inverted
          data={messages}
          keyExtractor={(message) => message.id}
          renderItem={renderMessage}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'flex-end',
          }}
        />
      </KeyboardAvoidingView>
      <InputBar sendMessage={sendMessage} />
      <Dialog.Container visible={visible}>
        <Dialog.Title>Save Bookmark</Dialog.Title>
        <Dialog.Description>
          Do you want to save this bookmark?
        </Dialog.Description>
        <Dialog.Input onChangeText={(input) => setSaveAs(input)} />
        <Dialog.Button label="Cancel" onPress={onPressCancel} />
        <Dialog.Button label="Save" onPress={onPressSave} />
      </Dialog.Container>
    </SafeAreaView>
  );
};

export default Lex;
